package com.lollipop50.moneywatcher.util

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.lollipop50.moneywatcher.Application

fun Context.getViewModelProvider(owner: ViewModelStoreOwner): ViewModelProvider {
    val appContext = applicationContext as Application
    val factory = appContext.viewModelFactory

    return ViewModelProvider(owner, factory)
}