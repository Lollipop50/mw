package com.lollipop50.moneywatcher.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lollipop50.moneywatcher.data.repository.AuthRepository
import com.lollipop50.moneywatcher.data.repository.MockAuthRepository
import com.lollipop50.moneywatcher.ui.splash.SplashViewModel

class ViewModelFactory : ViewModelProvider.Factory {

    private val authRepository: AuthRepository by lazy { MockAuthRepository() }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            SplashViewModel::class.java -> SplashViewModel(authRepository)

            else -> throw IllegalStateException("Found: $modelClass::class.java.name")
        } as T
    }
}