package com.lollipop50.moneywatcher

import android.app.Application
import com.lollipop50.moneywatcher.util.ViewModelFactory

class Application : Application() {

    val viewModelFactory = ViewModelFactory()
}