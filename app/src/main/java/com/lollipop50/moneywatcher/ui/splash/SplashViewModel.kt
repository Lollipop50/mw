package com.lollipop50.moneywatcher.ui.splash

import androidx.lifecycle.LiveData
import com.lollipop50.moneywatcher.data.repository.AuthRepository
import com.lollipop50.moneywatcher.ui.base.BaseViewModel
import com.lollipop50.moneywatcher.util.SingleLiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(private val authRepository: AuthRepository) : BaseViewModel() {

    private val authorizedEvent = SingleLiveEvent<Unit>()
    val authorizedLiveData: LiveData<Unit> = authorizedEvent

    private val notAuthorizedEvent = SingleLiveEvent<Unit>()
    val notAuthorizedLiveData: LiveData<Unit> = notAuthorizedEvent

    init {
        checkAuthorization()
    }

    private fun checkAuthorization() {
        scope.launch {
            val waitingJob = scope.launch {
                delay(2000)
            }

            val checkingAuthorizationJob = scope.async {
                authRepository.isAuthorized()
            }

            waitingJob.join()

            if (checkingAuthorizationJob.await()) {
                authorizedEvent.postValue(Unit)
            } else {
                notAuthorizedEvent.postValue(Unit)
            }
        }
    }
}