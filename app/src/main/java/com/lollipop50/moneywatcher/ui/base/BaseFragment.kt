package com.lollipop50.moneywatcher.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.lollipop50.moneywatcher.ui.progress.ProgressDialogFragment
import com.lollipop50.moneywatcher.util.getViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<VM : BaseViewModel>(
    @LayoutRes contentLayoutId: Int
) : Fragment(contentLayoutId) {

    protected val viewModel: VM by lazy {
        requireContext().getViewModelProvider(this).get(
            (javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        subscribeLiveData()
    }

    @CallSuper
    protected open fun subscribeLiveData() {}

    protected fun showProgress() {
        ProgressDialogFragment.show(childFragmentManager)
    }

    protected fun hideProgress() {
        ProgressDialogFragment.hide(childFragmentManager)
    }
}