package com.lollipop50.moneywatcher.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.lollipop50.moneywatcher.util.getViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : BaseViewModel>(
    @LayoutRes contentLayoutId: Int
) : AppCompatActivity(contentLayoutId) {

    protected val viewModel: VM by lazy {
        getViewModelProvider(this).get(
            (javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        subscribeLiveData()
    }

    @CallSuper
    protected open fun subscribeLiveData() {}
}