package com.lollipop50.moneywatcher.ui.progress

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.FragmentManager
import com.lollipop50.moneywatcher.R

class ProgressDialogFragment : AppCompatDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_progress_dialog, container, false)
    }

    companion object {

        private val FRAGMENT_TAG = ProgressDialogFragment::class.java.name

        fun newInstance() = ProgressDialogFragment()

        fun show(manager: FragmentManager) {
            if (manager.findFragmentByTag(FRAGMENT_TAG) == null) {
                val fragment = newInstance()
                fragment.show(manager, FRAGMENT_TAG)
            }
        }

        fun hide(manager: FragmentManager) {
            val fragment = manager.findFragmentByTag(FRAGMENT_TAG) as? ProgressDialogFragment
            fragment?.dismissAllowingStateLoss()
        }
    }
}