package com.lollipop50.moneywatcher.ui.splash

import android.os.Bundle
import android.view.animation.AnimationUtils
import com.lollipop50.moneywatcher.R
import com.lollipop50.moneywatcher.ui.base.BaseActivity
import com.lollipop50.moneywatcher.util.observe
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity<SplashViewModel>(R.layout.activity_splash) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fadeInLogoAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        appLogoImage.startAnimation(fadeInLogoAnimation)
    }

    override fun subscribeLiveData() {
        super.subscribeLiveData()

        viewModel.authorizedLiveData.observe(this) {
            TODO("Start TransactionsListActivity")
        }

        viewModel.notAuthorizedLiveData.observe(this) {
            TODO("Start AuthActivity")
        }
    }
}