package com.lollipop50.moneywatcher.ui.base

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.plus

abstract class BaseViewModel : ViewModel() {

    protected val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG, "Unhandled exception", exception)
    }

    protected val scope = viewModelScope + handler + Dispatchers.Default

    companion object {

        private val TAG = BaseViewModel::class.java.name
    }
}