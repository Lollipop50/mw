package com.lollipop50.moneywatcher.data.repository

interface AuthRepository {

    suspend fun isAuthorized(): Boolean
}