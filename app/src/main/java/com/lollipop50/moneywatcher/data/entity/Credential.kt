package com.lollipop50.moneywatcher.data.entity

data class Credential(val username: String, val password: String)