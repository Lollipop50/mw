package com.lollipop50.moneywatcher.data.repository

import com.lollipop50.moneywatcher.data.entity.Credential

class MockAuthRepository : AuthRepository {

    private val credential = Credential(username = "Lollipop50", password = "password")

    private var isAuthorized = false

    override suspend fun isAuthorized() = isAuthorized
}